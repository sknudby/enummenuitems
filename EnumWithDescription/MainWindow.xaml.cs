﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        MyEnum _selectedDirection;
        public MyEnum SelectedDirection
        {
            get { return _selectedDirection; }
            set { _selectedDirection = value; MessageBox.Show("set value to " + value); }
        }

    }

    public enum MyEnum
    {
        [Description("MyEnum1 Description")]
        MyEnum1,
        [Description("MyEnum2 Description")]
        MyEnum2,
        [Description("MyEnum3 Description")]
        MyEnum3,
        [Description("MyEnum4 Description")]
        MyEnum4
    }
}
