﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace WpfAppEnumMenuItems
{
    public class MenuItemCommand<T> : INotifyPropertyChanged
    {
        public ICommand Command { get; set; }
        public string Header { get; set; }
        public T Parameter { get; set; }
        public Func<T, bool> IsCheckedDelegate { private get; set; }
        public bool Checked
        {
            get { return IsCheckedDelegate(Parameter); }
            //private set { OnPropertyChanged(); }
        }

        public void UpdateIsChecked() {
            //Checked = Checked;
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Checked))); }

        public event PropertyChangedEventHandler PropertyChanged;
        //public void OnPropertyChanged([CallerMemberName] string propName = null)
        //{
        //    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        //}
    }
}
