﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfAppEnumMenuItems
{
    public enum ProcessType
    {
        [Description("A slow process")]
        Slow,
        [Description("A fast process")]
        Fast
    }
}
