﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Converters;

namespace WpfAppEnumMenuItems
{
    public class MainViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged([CallerMemberName] string propName = null)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }

        

        private ICollection<MenuItemCommand<ProcessType>> _commands;
        public ICollection<MenuItemCommand<ProcessType>> Commands
        {
            get { return _commands; }
        }

        private static ICollection<MenuItemCommand<T>> LoadCommands<T>(Action<T> command, Func<T, string> header, Func<T, bool> isChecked, PropertyChangedEventHandler propertyChanged)
        {
            var commands =   new List<MenuItemCommand<T>>();
            var relayCommand = new RelayCommand(o => command((T)o));
            var collection = CollectionFromEnum<T>();
            var converter = new EnumDescriptionConverter();
            foreach (var item in collection)
            {
                var menuItem = new MenuItemCommand<T>
                {
                    Command = relayCommand,
                    Parameter = item,
                    Header = header(item),
                    IsCheckedDelegate = isChecked
                };

                propertyChanged +=  (sender, e) => menuItem.UpdateIsChecked();
                commands.Add(menuItem);
            }
            return commands;
        }

        
        private void OnGoCommand(ProcessType obj)
        {
            Selected = obj;
        }

        public MainViewModel()
        {

            _commands = LoadCommands<ProcessType>(OnGoCommand,
                t => EnumDescriptionConverter.GetEnumDescription(t), IsProcessTypeSelected, PropertyChanged);

        }

        static ICollection<T> CollectionFromEnum<T>()
        {
            return ((T[])Enum.GetValues(typeof(T))).ToList();
        }

        ICollection<ProcessType> _list = ((ProcessType[])Enum.GetValues(typeof(ProcessType))).ToList();
        public ICollection<ProcessType> ProcessTypes { get { return _list; /*CollectionFromEnum<ProcessType>();*/ } }
        public bool IsProcessTypeSelected(ProcessType processType)
        {
            return processType == Selected;
        }

        public bool IsProcessTypeSelected()
        {
            return true;
        }
        private ProcessType _selected;
        public ProcessType Selected
        {
            get { return _selected; }
            set { _selected = value; OnPropertyChanged(); }
        }
        
        public ICommand ClickCommand
        {
            get
            {
                return new RelayCommand(o => { var val = (ProcessType)o; Selected = val; });
            }
        }


    }
    
}
